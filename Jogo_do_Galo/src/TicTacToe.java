import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * @author Bruno Augusto
 */
public class TicTacToe {
	static Scanner in;
	static String[] board;
	static String turn;
	
	public static void main(String[] args) {
		in = new Scanner(System.in);
		board = new String[9];
		turn = "X";
		String winner = null;
		populateEmptyBoard();

		System.out.println("Bem vindo ao Jogo Do Galo.");
		System.out.println("--------------------------");
		printBoard();
		System.out.println("'X' joga primeiro. Introduz um numero dos que ves no tabuleiro e ir� introduzir o 'X' nesse numero");
		while (winner == null) {
			/*
			 * No caso de inputs invalidos a seguinte funcao n o deixara prosseguir com o jogo
			 */
			int numInput;
			try {
				numInput = in.nextInt();
				if (!(numInput > 0 && numInput <= 9)) {
					System.out.println("Input invalido; Introduza denovo o numero");
					continue;
				}
			} catch (InputMismatchException e) {
				System.out.println("Input invalido; Introduza denovo o numero");
				continue;
			}
			if (board[numInput-1].equals(String.valueOf(numInput))) {
				board[numInput-1] = turn;
				if (turn.equals("X")) {
					turn = "O";
				} else {
					turn = "X";
				}
				printBoard();
				winner = checkWinner();
			} else {
				System.out.println("Este espa�o ja esta a ser utilizado escolha outro");
				continue;
			}
		}
		if (winner.equalsIgnoreCase("Empate")) {
			System.out.println("Acabou em empate, obrigado por jogar");
		} else {
			System.out.println("Parabens o " + winner + " ganhou! Obrigado por jogar");
		}
	}
	static String checkWinner() {
		/*
		 * Casos em que haver� um vencedor
		 */
		for (int a = 0; a < 8; a++) {
			String line = null;
			switch (a) {
			case 0:
				line = board[0] + board[1] + board[2];
				break;
			case 1:
				line = board[3] + board[4] + board[5];
				break;
			case 2:
				line = board[6] + board[7] + board[8];
				break;
			case 3:
				line = board[0] + board[3] + board[6];
				break;
			case 4:
				line = board[1] + board[4] + board[7];
				break;
			case 5:
				line = board[2] + board[5] + board[8];
				break;
			case 6:
				line = board[0] + board[4] + board[8];
				break;
			case 7:
				line = board[2] + board[4] + board[6];
				break;
			}
			if (line.equals("XXX")) {
				return "X";
			} else if (line.equals("OOO")) {
				return "O";
			}
		}
		
		for (int a = 0; a < 9; a++) {
			if (Arrays.asList(board).contains(String.valueOf(a+1))) {
				break;
			}
			else if (a == 8) return "empate";
		}

		System.out.println(turn + " introduza um numero;" + turn + " em:");
		return null;
	}
	static void printBoard() {
		/*
		 * Dar Print no Board do nosso jogo
		 */
		System.out.println("/---|---|---\\");
		System.out.println("| " + board[0] + " | " + board[1] + " | " + board[2] + " |");
		System.out.println("|-----------|");
		System.out.println("| " + board[3] + " | " + board[4] + " | " + board[5] + " |");
		System.out.println("|-----------|");
		System.out.println("| " + board[6] + " | " + board[7] + " | " + board[8] + " |");
		System.out.println("/---|---|---\\");
	}
	static void populateEmptyBoard() {
		/*
		 * D� nos os numero de 0 a 9 para popular o nosso board
		 */
		for (int a = 0; a < 9; a++) {
			board[a] = String.valueOf(a+1);
		}
	}
}