import java.util.*;
public class Exercico3 {
	private static Scanner scan;
	public static void main(String[] args) {
		int x1,x2,y1,y2;
		double distancia;
		scan = new Scanner (System.in);
		
		System.out.print("coordenadas dos Pontos 1 (x/y): ");
		x1 = scan.nextInt();
		y1 = scan.nextInt();
		
		System.out.print("coordenadas dos Pontos 2 (x/y): ");
		x2 = scan.nextInt();
		y2 = scan.nextInt();
		
		distancia = (Math.pow((y1 - x1),2) + Math.pow((y2 - x1),2));
		double rq = Math.sqrt(distancia);
		System.out.println(rq);
	}

}
