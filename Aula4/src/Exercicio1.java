import java.util.Scanner;

public class Exercicio1 {

	public static void main(String[] args) {

		int numero, i;
		double num;
		
		Scanner scan = new Scanner (System.in);
		System.out.print("Introduza numero: ");
		numero = scan.nextInt();
		
		num = 0;
		
		for (i = 1 ; i <= numero ; i = i + 1) {
			if (numero % i == 0 && i != 1 && i != numero) {
				num ++;
			}
		}
		
		if (num > 0) {
			System.out.println(numero + " n�o � primo");
		}else {
			System.out.println(numero + " � primo");
		}
	}

}
