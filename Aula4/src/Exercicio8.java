import java.util.Scanner;
public class Exercicio8 {

	public static void main(String[] args) {
		int CC, idade = 0, CCMin=0, idadeMin;

		idadeMin = 200;
		do {
			Scanner scan = new Scanner(System.in);
			System.out.print("Introduza CC: ");
			CC = scan.nextInt();
			if (CC != 0) {
				System.out.print("Introduza idade: ");
				idade = scan.nextInt();
			}

			if (idade < idadeMin) {
				idadeMin = idade;
				CCMin = CC;
			}
		} while (CC != 0);

		System.out.print("CC e " + CCMin + " idade da Pessoa: " + idadeMin);

	}

}