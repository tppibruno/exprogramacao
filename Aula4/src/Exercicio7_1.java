import java.util.Scanner;
public class Exercicio7_1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		double fat =1;

		
	
		System.out.println("Digite o numero que deseja obter o Fatorial:");
		fat = (double)sc.nextInt(); //F(5) = 1*2*3*4*5
		
		
		int factCalc=1;
		for (int i = 1; i <=fat ; i++){
			factCalc = factCalc * i;
		}
		
		System.out.println("Fatorial: "+ factCalc);

	}
}
