import java.util.Scanner;

public class Exercico6 {

	public static void main(String[] args) {
		int limInf, limSup;
		double resultado;
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Introduza numero lim inf: ");
		limInf = scan.nextInt();
		
		System.out.print("Introduza numero lim sup: ");
		limSup = scan.nextInt();
		
		if (limInf>limSup) {
			int temp = limSup;
			limSup = limInf;
			limInf = temp;
		}
		
		resultado = 0;
		for (int i = limInf; i<=limSup;i++){
			resultado=resultado+i;	
		}
		System.out.print("resultado: " +resultado);

	}

}
